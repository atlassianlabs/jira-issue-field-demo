module.exports = function (app, addon) {
    var _ = require('underscore');

    var store = require('./store.js');
    var sync = require('./sync.js')(app, addon);

    app.get('/team-list', addon.authenticate(), function (req, res) {
        res.render('teams');
    });

    app.get('/teams', addon.checkValidToken(), function (req, res) {
        store.getAllTeams().then(res.json.bind(res));
    });

    app.get('/teams/:id', addon.checkValidToken(), function (req, res) {
        store.getTeam(req.params.id).then(res.json.bind(res));
    });

    app.post('/teams', addon.checkValidToken(), function (req, res) {
        store.addTeam(req.body.value, req.body.category).then(res.json.bind(res));
    });

    app.put('/teams/:id', addon.checkValidToken(), function (req, res) {
        sync.updateTeam(req, parseInt(req.params.id)).then(function(doc) {
           res.json(doc);
        }, function(err) {
            console.log(err);
            res.send(500);
        });
    });

    app.delete('/teams/:id', addon.checkValidToken(), function (req, res) {
        console.log('removing entity with id', req.params.id);
        sync.removeTeam(req, parseInt(req.params.id)).then(function() {
            res.send(204);
        }, function() {
            res.send(404);
        });
    });
};
