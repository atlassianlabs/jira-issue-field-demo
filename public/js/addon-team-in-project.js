/* add-on script */
var JWT_TOKEN;

$(document).ready(function () {
    JWT_TOKEN = $('#token').attr('content');

    function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var projectId = getParameterByName("projectId");


    $( ".team aui-toggle" ).on( "change", function() {
        var toggle = this;
        var teamId = this.getAttribute('team-id');
        var isChecked = toggle.checked;     // new value of the toggle
        toggle.busy = true;

        if (isChecked) {
            $.post('add-team-to-project/'+teamId, { value: projectId })
                .done(function () {
                    console.log('success');
                })
                .fail(function () {
                    toggle.checked = !isChecked;
                    console.error('display an error message');
                })
                .always(function () {
                    toggle.busy = false;
                });

        } else {
            $.post('remove-team-from-project/'+teamId, { value: projectId })
                .done(function () {
                    console.log('success');
                })
                .fail(function () {
                    toggle.checked = !isChecked;
                    console.error('display an error message');
                })
                .always(function () {
                    toggle.busy = false;
                });
        }
    });

    $.ajaxSetup({
        headers: {'Authorization': "JWT " + JWT_TOKEN}
    });

    $( document ).ajaxSuccess(function( event, xhr, settings ) {
        refreshToken(xhr);
    });

    function refreshToken(jqXHR) {
        JWT_TOKEN = jqXHR.getResponseHeader('X-acpt');
    }
});
